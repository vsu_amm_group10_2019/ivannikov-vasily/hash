﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forms
{
    public partial class AddForm : Form
    {
        public Info info { get; set; } = new Info();
        public bool flag = false;
        public AddForm()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MaximizeBox = false;
            MinimizeBox = false;
        }
        private void Add_Click(object sender, EventArgs e)
        {
            flag = true;
            
            if (AddCardNum.Text != "")
                info.CardNum = Convert.ToInt32(AddCardNum.Text);
            else
                flag = false;
            
            if (AddFIO.Text != "")
                info.FIO = AddFIO.Text;
            else
                flag = false;
            
            if (AddSalary.Text != "")
                info.Salary = Convert.ToInt32(AddSalary.Text);
            else
                flag = false;
            
            Close();
        }

        private void AddCardNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }

        private void AddSalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 44 && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }
    }
}
