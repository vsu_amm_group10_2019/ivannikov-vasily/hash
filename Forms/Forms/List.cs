﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Forms
{
    public class List<T>
    {
        public Node<T> Head { get; set; }
        public Node<T> Tail { get; set; }
        public int Size { get; set; } = 0;
        public void Add(T info)
        {
            Node<T> node = new Node<T> { Info = info, Next = null };

            if (Head == null)
            { Head = node; }
            else
            {
                Tail.Next = node;
            }
            Tail = node;

            Size++;
        }

        public void Edit(Node<Info> head, Info info)
        {
            Node<Info> curr = head;
            do
            {
                if (curr.Info.CardNum == info.CardNum)
                {
                    curr.Info.FIO = info.FIO;
                    curr.Info.Salary = info.Salary;
                }
                curr = curr.Next;
            } while (curr != null);
        }
        public T Search(int index)
        {
            Node<T> curr = Head;
            for (int i = 0; i < index; i++)
            {
                curr = curr.Next;
            }
            return curr.Info;
        }
        public void Delete(int index)
        {
            if (index == 0)
            {
                Head = Head.Next;
                if (Size == 1)
                {
                    Tail = null;
                }
                Size -= 1;
                return;
            }
            Node<T> current = Head;
            Node<T> previous = null;
            for (int i = 0; i < index; i++)
            {
                previous = current;
                current = current.Next;
            }

            previous.Next = current.Next;
        }
        public void Clear()
        {
            Head = null;
            Tail = null;
            Size = 0;
        }
    }
}
