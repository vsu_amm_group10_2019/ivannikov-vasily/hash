﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

namespace Forms
{
    public partial class Form1 : Form
    {
        int Size = 0;
        private string FileName { get; set; }
        const int SizeTable = 100;
        HashTable hashTable { get; set; } = new HashTable(SizeTable);
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
        private void Refresh()
        {
            dataGridInfo.Rows.Clear();
            dataGridInfo.RowCount = Size;
            int pos = 0;
            if (Size != 0)
            {
                for (int i = 0; i < SizeTable; i++)
                {
                    if (hashTable.Get(i) != null)
                    {
                        Node<Info> node = hashTable.Get(i);
                        do
                        {
                            dataGridInfo.Rows[pos].Cells[0].Value = node.Info.CardNum;
                            dataGridInfo.Rows[pos].Cells[1].Value = node.Info.FIO;
                            dataGridInfo.Rows[pos].Cells[2].Value = node.Info.Salary;
                            pos++;
                            node = node.Next;
                        } while (node != null);
                    }
                }
            }
        }
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm();
            addForm.ShowDialog();
            if (addForm.info.FIO != null && addForm.info.CardNum != null && addForm.info.Salary != null)
            {
                if (addForm.flag == true)
                {
                    hashTable.Add(addForm.info);
                    Size++;
                    Refresh();
                }
                else
                {
                    MessageBox.Show("Ошибка при добавлении данных");
                }
            }
            else
            {
                MessageBox.Show("Ошибка при добавлении данных");
            }
        }

        private void editToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            AddForm addForm = new AddForm();
            addForm.ShowDialog();
            hashTable.Edit(addForm.info);
            Refresh();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Edit edit = new Edit();
            edit.ShowDialog();
            hashTable.Delete(edit.digit);
            Size--;
            Refresh();
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Size = 0;
            Refresh();
            for (int i = 0; i < SizeTable; i++)
            {
                hashTable.Clear(i);
            }
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Edit edit = new Edit();
            edit.ShowDialog();
            hashTable.Search(edit.digit);
        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
                SaveToFile();

            openFileDialog.Filter = "Json File|*.json";
            openFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(openFileDialog.FileName))
            {
                FileName = openFileDialog.FileName;
                string txt = File.ReadAllText(FileName);
                List<Info> students = JsonConvert.DeserializeObject<List<Info>>(txt);
                
                for (int i = 0; i < SizeTable; i++)
                    hashTable.Clear(i);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            else
            {
                saveAsToolStripMenuItem_Click(sender, e);
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        private void SaveToFile()
        {
            List<Info> students = new List<Info>();

            for (int i = 0; i < SizeTable; i++)
            {
                Node<Info> nodeHead = hashTable.Get(i);
                while (nodeHead != null)
                {
                    students.Add(nodeHead.Info);
                    nodeHead = nodeHead.Next;
                }   
            }

            string result = JsonConvert.SerializeObject(students);
            File.WriteAllText(FileName, result);
        }
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(FileName))
                SaveToFile();

            FileName = "";
            for (int i = 0; i < SizeTable; i++)
                hashTable.Clear(i);
            Refresh();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Json File|*.json";
            saveFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(saveFileDialog.FileName))
            {
                FileName = saveFileDialog.FileName;
                SaveToFile();
            }
        }
    }
}
