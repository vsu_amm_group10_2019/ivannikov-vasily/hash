﻿
namespace Forms
{
    partial class AddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.AddCardNum = new System.Windows.Forms.TextBox();
            this.AddFIO = new System.Windows.Forms.TextBox();
            this.AddSalary = new System.Windows.Forms.TextBox();
            this.Add = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Card Number";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "FIO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Salary";
            // 
            // AddCardNum
            // 
            this.AddCardNum.Location = new System.Drawing.Point(93, 24);
            this.AddCardNum.Name = "AddCardNum";
            this.AddCardNum.Size = new System.Drawing.Size(175, 20);
            this.AddCardNum.TabIndex = 4;
            this.AddCardNum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AddCardNum_KeyPress);
            // 
            // AddFIO
            // 
            this.AddFIO.Location = new System.Drawing.Point(93, 84);
            this.AddFIO.Name = "AddFIO";
            this.AddFIO.Size = new System.Drawing.Size(175, 20);
            this.AddFIO.TabIndex = 5;
            // 
            // AddSalary
            // 
            this.AddSalary.Location = new System.Drawing.Point(93, 148);
            this.AddSalary.Name = "AddSalary";
            this.AddSalary.Size = new System.Drawing.Size(175, 20);
            this.AddSalary.TabIndex = 6;
            this.AddSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AddSalary_KeyPress);
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(114, 199);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(75, 23);
            this.Add.TabIndex = 7;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // AddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 247);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.AddSalary);
            this.Controls.Add(this.AddFIO);
            this.Controls.Add(this.AddCardNum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add student";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox AddCardNum;
        private System.Windows.Forms.TextBox AddFIO;
        private System.Windows.Forms.TextBox AddSalary;
        private System.Windows.Forms.Button Add;
    }
}