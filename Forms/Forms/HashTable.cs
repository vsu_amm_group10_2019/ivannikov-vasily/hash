﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Forms
{
    public class HashTable
    {
        private readonly int Size;
        private List<Info>[] Table;
        public HashTable(int size)
        {
            Size = size;
            Table = new List<Info>[Size];
        }

        public bool Add(Info info)
        {
            int index = info.CardNum % Size;
            if (Table[index] == null)
            {
                Table[index] = new List<Info>();
            }
            if (info != null)
            {
                Table[index].Add(info);
                return true;
            }
            else
            {
                return false;
            }
        }

        public string Find(int index)
        {
            string res = " ";
            for (int i = 0; i < Size; i++)
            {
                if (index % Size == i)
                {
                    Node<Info> curr = Table[i].Head;
                    do
                    {
                        if (curr.Info.CardNum == index)
                        {
                            res += Convert.ToString(curr.Info.CardNum) + ' ' + curr.Info.FIO + ' ' + curr.Info.Salary + ' ' + "\n";
                        }
                        curr = curr.Next;
                    } while (curr != null);
                }
            }
            return res;
        }

        public string ToString()
        {
            string res = "{ \n";
            for (int i = 0; i < Size; i++)
            {
                if (Table[i] != null)
                {
                    Node<Info> curr = Table[i].Head;
                    while (curr != null)
                    {
                        res += Convert.ToString(curr.Info.CardNum) + ' ' + curr.Info.FIO + ' ' + curr.Info.Salary + ' ' + "\n";
                        curr = curr.Next;
                    }
                }
            }
            res += "}";
            return res;
        }

        public Node<Info> Get(int index)
        {
            if (Table[index] != null)
            {
                return Table[index].Head;
            }
            else
            {
                return null;
            }
        }
        public void Edit(Info info)
        {
            int hash = info.CardNum % Size;
            List<Info> list = Table[hash];
            list.Edit(list.Head, info);
        }

        public void Search(int index)
        {
            int hash = index % Size;
            List<Info> list = Table[hash];
            for (int i = 0; i <= list.Size; i++) 
            {
                Info info = list.Search(i);
                if (info.CardNum == index)
                {
                    MessageBox.Show("Номер: " + Convert.ToString(info.CardNum) + " ФИО: " + info.FIO +
                                    " Зарплата: " + Convert.ToString(info.Salary));
                    return;
                }
            }
        }
        public void Delete(int index)
        {
            int hash = index % Size;
            List<Info> list = Table[hash];
            for (int i = 0; i <= list.Size; i++)
            {
                Info info = list.Search(i);
                if (info.CardNum == index)
                {
                    list.Delete(i);
                    return;
                }
            }
        }
        public void Clear(int index)
        {
            if (Table[index] != null) 
            {
                List<Info> list = Table[index];
                list.Clear(); 
            }
        }
    }
}
