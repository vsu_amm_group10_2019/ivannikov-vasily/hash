﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Forms
{
    public class Info
    {
        public int CardNum { get; set; }
        public string FIO { get; set; }
        public int Salary { get; set; }
    }
}
