﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            const int max = 100;
            HashTable hashTable = new HashTable(max);

            hashTable.Add(new Info { CardNum = 1, FIO = "Student 1", Salary = 10000 });
            hashTable.Add(new Info { CardNum = 101, FIO = "Student 101", Salary = 101000 });
            hashTable.Add(new Info { CardNum = 2, FIO = "Student 2", Salary = 20000 });

            Console.WriteLine(hashTable.ToString());
        }
    }
}
