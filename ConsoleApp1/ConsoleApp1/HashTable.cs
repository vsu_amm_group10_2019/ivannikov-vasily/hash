﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class HashTable
    {
        private readonly int Size;
        private List<Info>[] Table;
        public HashTable(int size)
        {
            Size = size;
            Table = new List<Info>[Size];
        }

        public void Add(Info info)
        {
            int index = info.CardNum % Size;
            if (Table[index] == null)
            {
                Table[index] = new List<Info>();
            }
            Table[index].Add(info);
        }

        public string Find(int index)
        {
            string res = " ";
            for (int i = 0; i < Size; i++)
            {
                if (index % Size == i)
                {
                    Node<Info> curr = Table[i].Head;
                    do
                    {
                        if (curr.Info.CardNum == index)
                        {
                            res += Convert.ToString(curr.Info.CardNum) + ' ' + curr.Info.FIO + ' ' + curr.Info.Salary + ' ' + "\n";
                        }
                        curr = curr.Next;
                    } while (curr != null);
                }
            }
            return res;
        }

        public string ToString()
        {
            string res = "{ \n";
            for (int i = 0; i < Size; i++)
            {
                if (Table[i] != null)
                {
                    Node<Info> curr = Table[i].Head;
                    while (curr != null)
                    {
                        res += Convert.ToString(curr.Info.CardNum) + ' ' + curr.Info.FIO + ' ' + curr.Info.Salary + ' ' + "\n";
                        curr = curr.Next;
                    }
                }
            }
            res += "}";
            return res;
        }
        public void Delete(int index)
        {
            int hash = index % Size;
            List<Info> list = Table[hash];
            list.Delete(list.Head, index);
        }
        public void Clear(int index)
        {
            List<Info> list = Table[index];
            list.Clear();
        }
    }
}
