﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Node<T>
    {
       public T Info { get; set; }
       public Node<T> Next { get; set; }
    }
}
